//////////////////////////////////////////////////////////////////////////////
class scrollingHLabel extends scrollingText{
  constructor(index, l1, l2, txtSize, speed, color) { // 6, 'top down', 'emergent', 600, color(255,255,0))
    super(l1+"-"+l2, txtSize, font1, speed, color);
    this.l1 = l1;
    this.l2 = l2;
    this.index = index;
    hWidth += this.bounds.w;
  }

  scroll() {
    this.x += this.speed;

    if (this.x > hPaddedWidth-100)
      this.x = - 100;
  }

  clicked(px, py) {
    if(super.over(px, py)) {
      lL = this.l1;
      lR = this.l2;
      currentH = this.index;
      console.log("current H: " + currentH);
    }
  }
  resize(){
    this.x = hPaddedWidth;
    hPaddedWidth += this.bounds.w + hPadding;

    this.y = pistaHlabels; //this.y = height-pistaHlabels+17;
  }
}
class scrollingVLabel extends scrollingText{
  constructor(index, l1, l2, txtSize, speed, color) { // 6, 'top down', 'emergent', 600, color(255,255,0))
    super(l1+"-"+l2, txtSize, font1, speed, color);
    this.l1 = l1;
    this.l2 = l2;
    this.index = index;
    vWidth += this.bounds.w;
  }

  scroll() {
    this.x += this.speed;

    if (this.x > vPaddedWidth-100)
      this.x = - 100;
  }

  clicked(px, py) {
    if(super.over(px, py)) {
      lT = this.l1;
      lB = this.l2;
      currentV = this.index;
      console.log("current V: " + currentV);
    }
  }
  resize(){
    this.x = vPaddedWidth;
    vPaddedWidth += this.bounds.w + vPadding;

    this.y = pistaVlabels; //this.y = height-pistaVlabels+17;
  }
}
