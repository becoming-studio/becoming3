//////////////////////////////////////////////////////////////////////////////
class scrollingNew extends scrollingText{
  constructor(text, txtSize, speed, color, link) { // 6, 'top down', 'emergent', 600, color(255,255,0))
    super(text, txtSize, font2, speed, color);
    this.link = link;
    nWidth += this.bounds.w;
  }

  scroll() {
    this.x -= this.speed;

    if (this.x < -this.bounds.w)
      this.x = width + nPaddedWidth - this.bounds.w;
  }

  clicked(px, py) {
    if (this.link != 'null'){
      if(super.over(px, py)) {
        window.open(this.link, "_blank");
      }
    }
  }
  resize(){
    this.x = nPaddedWidth;
    nPaddedWidth += this.bounds.w + nPadding;

    this.y = pistaDisplay+17;
  }
}
