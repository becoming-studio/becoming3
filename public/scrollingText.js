class scrollingText{
  constructor(text, txtSize, font, speed, color) { // 6, 'top down', 'emergent', 600, color(255,255,0))
    this.text = text;
    this.txtSize = txtSize;
    this.font = font;
    this.x = 0;
    this.y = 0;
    this.speed = speed;
    this.color = color;
    this.bounds = this.font.textBounds(this.text, 0, 0, txtSize);
  }

  over(px, py){
    if( this.y-py>0 && this.y-py<this.bounds.h && px-this.x>0 && px-this.x<this.bounds.w) {
    //if( abs(this.y-py)<this.bounds.h/2 && abs(px-this.x)<this.bounds.w/2 ) { // + textAlign(CENTER);
      cross++;
      return true;
    }
    return false;
  }

  draw() {
    noStroke();
    fill(this.color);
    textSize(this.txtSize);
    textAlign(LEFT);
    textFont(this.font);
    text(this.text, this.x, this.y);
  }

  update(px, py){
    this.scroll();
    this.draw();
    this.over(px, py);
  }
}
