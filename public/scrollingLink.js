//NEWS/////////////////////////////////////////////////////////////////////////
class scrollingLink extends scrollingText{
  constructor(text, txtSize, speed, color, link, target) { // 6, 'top down', 'emergent', 600, color(255,255,0))
    super(text, txtSize, font3, speed, color);
    this.link = link;
    this.target = target;
    mWidth += this.bounds.w;
  }

  scroll() {
    this.x += this.speed;
    /// if (this.x > width + 1)
    if (this.x > mPaddedWidth-100)
      this.x = -100;
  }

  clicked(px, py) {
    if(super.over(px, py)) {
      if (this.target == '_self')
        window.location.replace(this.link);
      else
        window.open(this.link);
    }
  }
  resize(){
    this.x = mPaddedWidth;
    mPaddedWidth += this.bounds.w + mPadding;
    this.y = height-pistaMenu;
  }
}
