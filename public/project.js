class Project {
  constructor(name, imagePath, number, link, positions) {
    this.text = name;
    //this.image = loadImage(imagePath);

    let s = "000" + number;
    this.gif = loadAnimation(imagePath + 'frame_001.png', imagePath + 'frame_'+s.substr(s.length-3)+'.png');
    //console.log(this.image);
    this.link = link;
    this.txtSize = txtSizeM;
    this.positions = positions;
    this.bounds = {"w":0, "h":0}; // console.log(this.bounds);
    this.tbounds = {"w":0, "h":0}; //

    this.aim = createVector(this.positions[currentH], this.positions[currentV]);
    this.position = createVector(this.aim.x, this.aim.y);
    this.isOver = false;
  }

  canvasPosition(v){
    return createVector((v.x/100)*(width-100)+50, (v.y/100)*(height-200)+100);
  }

  over(px, py){
    let p = this.canvasPosition(this.position);
    //if( py>p.y && py-p.y<this.bounds.h && px>p.x && px-p.x<this.bounds.w) {
    if( Math.abs(py-p.y)<this.bounds.h/2 && Math.abs(px-p.x)<this.bounds.w/2 ) {

      //console.log(this.text + " over!");
      cross++;
      this.isOver = true;
      return true;
    }
    this.isOver = false;
    return false;
  }

  draw() {
    //console.log("drawing project!");
    let p = this.canvasPosition(this.position);
    //image(this.image, p.x, p.y);
    animation(this.gif, p.x, p.y);

    if (this.isOver){
      noStroke();
      fill(0);
      rect(mouseX+2, mouseY-2, this.tbounds.w + 25, -this.tbounds.h -5);
      textSize(this.txtSize);
      textAlign(LEFT);
      fill(255);
      textFont(font2);
      text(this.text, mouseX+5, mouseY-5);
    }
  }

  move(){

    let step = createVector(this.aim.x, this.aim.y)
    step.sub(this.position.x, this.position.y);
    step.limit(3);
    this.position.add(step);

  }

  update(px, py){
    if (this.gif.getImageAt(0).width > 1){
      this.bounds.w = this.gif.getImageAt(0).width;
      this.bounds.h = this.gif.getImageAt(0).height;
      this.tbounds = font2.textBounds(this.text, 0, 0, this.txtSize);
    }

    this.move();
    this.draw();
    this.over(px, py);
    this.aim.set(this.positions[currentH]*100, this.positions[currentV]*100);
  }

  clicked(px, py) {
    if(this.over(px, py)) {
      window.open(this.link, "_top");
    }
  }

  resize(){
    // only need to be called once (not even every resize time)

    // this.bounds.w = this.image.width;
    // this.bounds.h = this.image.height;
    //this.tbounds = font.textBounds(this.text, 0, 0, txtSize);

    this.position.set(this.aim.x, this.aim.y);
  }

}
