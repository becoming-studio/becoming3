//becoming 2019

// margins
let cross = 0;
let tM = 140;
let bM = 90;
let vC = 0;

let pistaDisplay = 10;
let pistaMenu = 15;
let pistaHlabels = 60;
let pistaVlabels = 90;

// arrays of interactive texts
let items = [];
let news = [];
let menu = [];
let hLabels = [];
let vLabels = [];
let currentH;
let currentV;

let nWidth = mWidth = hWidth = vWidth = 0; // news/menus/hlabels/vlabels aggregated width in pixels
let nPaddedWidth = mPaddedWidth = hPaddedWidth = vPaddedWidth = 0;
let nPadding= mPadding = hPadding = vPadding = 0;

let lT, lB, lR, lL;

let txtSizeXL = 22;
let txtSizeL = 18;
let txtSizeM = 16;
let txtSizeS = 14;

let font1, font2, font3;

let icons = [];
let projectData;
let newsData;
let projects = [];

let drawn = false;


function preload() {

  font1 = loadFont("assets/GangsterGrotesk-Light.otf"); //loadFont("assets/NotoSerif-Regular.ttf");
  font2 = loadFont("assets/GangsterGrotesk-Light.otf"); //loadFont("assets/Comic-Sans.ttf");
  font3 = loadFont("assets/GangsterGrotesk-Light.otf"); //loadFont("assets/Michroma-Regular.ttf");

  projectData = loadJSON('assets/projects.json'); // , createprojects
  newsData = loadJSON('assets/news-banner.json');
  console.log("done preloading!");
}

function createnews(nD){

  for (let n of nD.news){
    news.push(  new scrollingNew(  n.text, txtSizeM, 2, color(n.c[0], n.c[1], n.c[2]), n.link ) );
  }
}

function createprojects(pD){

  //choose a random apr of axis (labels)
  currentH = int(random(pD.labels.length));
  currentV = int(random(pD.labels.length));

  lL = pD.labels[currentH].l1;
  lR = pD.labels[currentH].l2;

  lT = pD.labels[currentV].l1;
  lB = pD.labels[currentV].l2;

  for (let p of pD.projects){
    projects.push( new Project(p.name, p.image, p.number, "https://becoming.network" + p.link, p.positions)); //https://becoming.network
  }
  console.log("done loading labels+projects!");
}

////////////////////////////////////////////////////////////////////////////////

function setup() {

  console.log("setting up...");
  pixelDensity(1);
  createCanvas(windowWidth,windowHeight);
  console.log(pixelDensity());

  angleMode(DEGREES);
  frameRate(30);
  strokeWeight(1);
  textFont(font1);


  //txtSize = 14; ///TODO parametrize

  vC = windowHeight/2 - tM/2  + bM/2;
  //lT = lB = lR = lL = "";

  createnews(newsData);
  createprojects(projectData);

  menu.push(  new scrollingLink(  "ABOUT", txtSizeL, 1.5, color(255), "https://becoming.network" + "/about.html", "_self" ) );
  menu.push(  new scrollingLink(  "NOSOTRES", txtSizeL, 1.5, color(255), "https://becoming.network" + "/whoweare.html", "_self" ) );
  menu.push(  new scrollingLink(  "CONTACTO", txtSizeL, 1.5, color(255), "https://becoming.network" + "/contact.html", "_self" ) );
  //menu.push(  new scrollingLink(  "MASTODON", txtSizeL, 1.5, color(255), "https://mastodon.social/@becoming" ) );
  menu.push(  new scrollingLink(  "TWITTER", txtSizeL, 1.5, color(255), "https://twitter.com/b3c0ming", "_blank" ) );
  menu.push(  new scrollingLink(  "INSTAGRAM", txtSizeL, 1.5, color(255), "https://instagram.com/b3c0mingc?igshid=12kp74xeq3wb5", "_blank" ) );
  menu.push(  new scrollingLink(  "LIBRE SOURCE", txtSizeL, 1.5, color(255), "https://gitlab.com/becoming-studio", "_blank" ) );

  for (let item of projectData.labels){
    hLabels.push(  new scrollingHLabel( item.i, item.l1, item.l2, txtSizeS, 1, color(255,255,255))  );
    vLabels.push(  new scrollingVLabel( item.i, item.l1, item.l2, txtSizeS, 1.2, color(255,255,255))  );
  }

  shuffle(hLabels, true);
  shuffle(vLabels, true);
  items = news.concat(menu).concat(hLabels).concat(vLabels).concat(projects);

  calcPaddings();
  for (let item of items) {
    item.resize();
  }
}
////////////////////////////////////////////////////////////////////////////////

function draw() {
  background(0);

  // AXIS

  drawAxis();
  for (let item of items) {
    item.update(mouseX, mouseY);
  }

  drawAxisLabels();

  // CURSOR  console.log(cross);
  if (cross > 0)  cursor(CROSS);
  else            cursor(ARROW);
  cross = 0;

  drawLayout();

  // framerate
    textSize(9); textAlign(LEFT); fill(255);
    text(int(frameRate()), width-25, height-55);

  if (drawn == false){
    // console.log("drawn " + drawn);
    drawn = true;
    resizeCanvas(windowWidth, windowHeight);

    vC = windowHeight/2 - bM/2  + tM/2;

    nPaddedWidth = mPaddedWidth = hPaddedWidth = vPaddedWidth = 0; // pre-reset counters

    calcPaddings();
    for (let item of items) {
      item.resize();
    }
  }
}


function mousePressed() {
  for (let item of items) {
    item.clicked(mouseX, mouseY);
  }
}

////////////////////////////////////////////////////////////////////////////////
function drawLayout(){

  fill(0);
  noStroke();

  rect(0,0,105,40);
  rect(0,40,50,30);
  rect(0,70,50,30);

  stroke(255,255,255);

  line(0, 40, width, 40);
  drawMarker(0, 40, 90);

  line(0, 70, width, 70);
  drawMarker(40, 70, 30);

  line(0, 100, width, 100);
  drawMarker(70, 100, 30);

  line(0, height-40, width, height-40);

  textSize(txtSizeM);
  textAlign(LEFT);

  noStroke();
  fill(255);
  text('becoming', 10, 25);
  text('X', 10, 60);
  text('Y', 10, 90);
  stroke(255);
}

function drawMarker(y1, y2, pos){

  line(pos, y1, pos+7, y1+(y2-y1)/2);
  line(pos+7, y1+(y2-y1)/2, pos, y2);

}
////////////////////////////////////////////////////////////////////////////////
function drawAxisLabels(){
  //noStroke();
  textSize(txtSizeM);
  textAlign(CENTER);

  // axis labels top & bottom
  fill(255,255,255);
  text(lT, width/2, tM - 10);
  text(lB, width/2, height - bM + 20);

  // axis label left
  push();
    translate(50 - 15, vC);
    rotate(- 90);
    text(lL, 0, 0);
  pop();

  // axis label right
  push();
    translate(width - 50 + 15, vC);
    rotate(90);
    text(lR, 0, 0);
  pop();

}

function drawAxis(){

  let arrowSize = 5;
  vC = ((height-bM)-tM)/2 + tM;  // to -> windoresized

  // thin lean sneak version
  //stroke(255,255,255);
  fill(255,255,255);
  // X
  line( 54, vC, 57, vC-5);
  line( 54, vC, 57, vC+5);
  line( 54, vC, width- 54, vC); // line
  line( width - 54, vC, width - 57, vC-5);
  line( width - 54, vC, width - 57, vC+5);
  // Y
  line( width/2, tM, width/2-5, tM+3);
  line( width/2, tM, width/2+5, tM+3);
  line(width/2, tM, width/2, height - bM); // line
  line( width/2, height - bM, width/2-5, height - bM - 3);
  line( width/2, height - bM, width/2+5, height - bM - 3);
}


function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
   //;windowHeight/2 - bM/2  + tM/2;

  vC = windowHeight/2 - tM/2  + bM/2;

  nPaddedWidth = mPaddedWidth = hPaddedWidth = vPaddedWidth = 0; // pre-reset counters

  calcPaddings();
  for (let item of items) {
    item.resize();
  }

  cameraReset();
}

function calcPaddings(){
  nPadding = 30; //(width>1.5*nWidth) ? (width-nWidth +00)/(news.length) : 30 ;
  mPadding = (width>1.25*mWidth) ? (width-mWidth +200)/(menu.length) : 30 ; //(width-mWidth + 100)/(menu.length);
  hPadding = (width>1.25*hWidth) ? (width-hWidth +200)/(hLabels.length) : 30 ;
  vPadding = (width>1.25*vWidth) ? (width-vWidth +200)/(vLabels.length) : 30 ;
}

function cameraReset(){
  this.camera.position.x = width/2;
  this.camera.position.y = height/2;
}
